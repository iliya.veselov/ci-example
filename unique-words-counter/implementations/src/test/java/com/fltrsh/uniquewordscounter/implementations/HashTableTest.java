package com.fltrsh.uniquewordscounter.implementations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.epm.lab.collections.Map;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;

public class HashTableTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void puttingAndGettingSingleElement() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertEquals(1, map.size());
        assertEquals(1, (int) map.get(1));
    }

    @Test
    public void puttingAndGettingTwoElements() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertEquals(1, map.size());
        //WHEN
        map.put(3, 4);
        //THEN
        assertEquals(2, map.size());
        assertEquals(1, (int) map.get(1));
        assertEquals(4, (int) map.get(3));
    }

    @Test
    public void puttingAndGettingFourElements() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertEquals(1, map.size());
        //WHEN
        map.put(3, 4);
        assertEquals(2, map.size());
        //WHEN
        map.put(2, 5);
        //THEN
        assertEquals(3, map.size());
        //WHEN
        map.put(4, 10);
        //THEN
        assertEquals(4, map.size());
        assertEquals(1, (int) map.get(1));
        assertEquals(5, (int) map.get(2));
        assertEquals(4, (int) map.get(3));
        assertEquals(10, (int) map.get(4));
    }

    @Test
    public void puttingAndGettingAnotherFourElements() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(5, 1);
        //THEN
        assertEquals(1, map.size());
        //WHEN
        map.put(1, 4);
        assertEquals(2, map.size());
        //WHEN
        map.put(3, 5);
        //THEN
        assertEquals(3, map.size());
        //WHEN
        map.put(2, 10);
        //THEN
        assertEquals(4, map.size());
        assertEquals(1, (int) map.get(5));
        assertEquals(4, (int) map.get(1));
        assertEquals(5, (int) map.get(3));
        assertEquals(10, (int) map.get(2));
    }

    @Test
    public void overrideValue() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertEquals(1, map.size());
        assertEquals(1, (int) map.get(1));
        //WHEN
        map.put(4, 2);
        //THEN
        assertEquals(2, map.size());
        assertEquals(2, (int) map.get(4));
        //WHEN
        map.put(4, 123);
        //THEN
        assertEquals(2, map.size());
        assertEquals(123, (int) map.get(4));
    }

    @Test
    public void gettingUnexistingElement() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertNull(map.get(2));
    }

    @Test
    public void gettingAnotherUnexistingElementt() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN
        map.put(1, 1);
        //THEN
        assertNull(map.get(-1));
    }

    @Test
    public void puttingElementWithNullValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = 1 value = null");
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN THEN
        map.put(1, null);
    }

    @Test
    public void puttingElementWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = null value = 1");
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN THEN
        map.put(null, 1);
    }

    @Test
    public void puttingElementWithNullKeyAndValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = null value = null");
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN THEN
        map.put(null, null);
    }

    @Test
    public void gettingElementWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. The key is null.");
        HashTable<Integer, Integer> map = new HashTable<>();
        //WHEN THEN
        map.get(null);
    }

    @Test
    public void testingIterator() {
        //GIVEN
        HashTable<Integer, Integer> map = new HashTable<>();
        int key = 1;
        int value = 1;
        //WHEN
        map.put(1, 1);
        map.put(2, 2);
        map.put(3, 3);
        map.put(4, 4);
        map.put(5, 5);
        //THEN
        for (Map.Entry<Integer, Integer> entry : map) {
            assertEquals(key++, (int) entry.key);
            assertEquals(value++, (int) entry.value);
        }
    }
}
