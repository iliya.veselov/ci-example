package com.fltrsh.uniquewordscounter.implementations;


import com.fltrsh.uniquewordscounter.interfaces.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class IndexedListTest {
    private List<Integer> list;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void initializeList() {
        list = new IndexedList<>();
    }

    @Test
    public void addingNullElement() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("You cannot insert null element.");
        Integer element = null;

        list.add(0, null);
    }

    @Test
    public void addingAndGettingFirstElement() {
        int index = 0;
        Integer addedElement = 5;

        list.add(index, addedElement);
        Integer recievedElement = list.get(index);

        Assert.assertEquals(recievedElement, addedElement);
    }

    @Test
    public void addingAndGettingFirstElementTwice() {
        Integer[] addedElements = {1, 2};
        Integer[] expectedRecievedElements = {2, 1};
        Integer[] actualRecievedElements = new Integer[2];

        list.add(0, addedElements[0]);
        list.add(0, addedElements[1]);
        actualRecievedElements[0] = list.get(0);
        actualRecievedElements[1] = list.get( 1);

        Assert.assertArrayEquals(expectedRecievedElements, actualRecievedElements);
    }

    @Test
    public void addingAndGettingElementsInMiddle() {
        Integer[] addedElements = {1, 2, 3};
        Integer[] expectedRecievedElements = {1, 3, 2};
        Integer[] actualRecievedElements = new Integer[3];

        list.add(0, addedElements[0]);
        list.add(1, addedElements[1]);
        list.add(1, addedElements[2]);
        actualRecievedElements[0] = list.get(0);
        actualRecievedElements[1] = list.get(1);
        actualRecievedElements[2] = list.get(2);

        Assert.assertArrayEquals(expectedRecievedElements, actualRecievedElements);
    }


    @Test
    public void addingAndGettingLastElement() {
        Integer[] addedElements = {1, 2};
        Integer[] expectedRecievedElements = {1, 2};
        Integer[] actualRecievedElements = new Integer[2];

        list.add(0, addedElements[0]);
        list.add(1, addedElements[1]);
        actualRecievedElements[0] = list.get(0);
        actualRecievedElements[1] = list.get(1);

        Assert.assertArrayEquals(expectedRecievedElements, actualRecievedElements);
    }

    @Test
    public void addingElementWithIndexLesserThanSize() {
        int index = -1;
        Integer addedElement = 5;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because index < 0 ");

        list.add(index, addedElement);
    }

    @Test
    public void addingElementWithIndexBiggerThanSize() {
        int size = list.size();
        int index = 1;
        Integer addedElement = 5;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because size = " + size);

        list.add(index, addedElement);
    }

    @Test
    public void gettingElementWithIndexLesserThanSize() {
        int index = -1;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because index < 0 ");

        list.get(index);
    }

    @Test
    public void gettingElementWithIndexBiggerThanSize() {
        int size = list.size();
        int index = 1;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because size = " + size);

        list.get(index);
    }

    @Test
    public void removingElementWithIndexLesserThanSize() {
        int index = -1;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because index < 0 ");

        list.remove(index);
    }

    @Test
    public void removingElementWithIndexBiggerThanSize() {
        int size = list.size();
        int index = 1;

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because size = " + size);

        list.remove(index);
    }

    @Test
    public void settingElementWithIndexLesserThanSize() {
        int index = -1;
        Integer settedElement = 5;
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because index < 0 ");

        list.set(index, settedElement);
    }

    @Test
    public void settingElementWithIndexBiggerThanSize() {
        int size = list.size();
        int index = 1;
        Integer settedElement = 5;
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Incorrect index = " + index + ", because size = " + size);

        list.set(index, settedElement);
    }

    @Test
    public void addingAndRemovingFirstElementTwice() {
        int index = 0;
        Integer[] addedElements = {1, 2};
        Integer[] expectedRemovedElements = {2, 1};
        Integer[] actualRemovedElements = new Integer[2];

        list.add(index, addedElements[0]);
        list.add(index, addedElements[1]);
        actualRemovedElements[0] = list.remove(index);
        actualRemovedElements[1] = list.remove(index);

        Assert.assertArrayEquals(expectedRemovedElements, actualRemovedElements);
    }

    @Test
    public void addingAndRemovingElementsInMiddle() {
        Integer[] addedElements = {1, 2, 3, 4};
        Integer[] expectedRemovedElements = {4, 3, 1, 2};
        Integer[] actualRemovedElements = new Integer[4];

        list.add(0, addedElements[0]);
        list.add( 1, addedElements[1]);
        list.add( 2, addedElements[2]);
        list.add( 1, addedElements[3]);
        actualRemovedElements[0] = list.remove( 1);
        actualRemovedElements[1] = list.remove( 2);
        actualRemovedElements[2] = list.remove(0);
        actualRemovedElements[3] = list.remove(0);

        Assert.assertArrayEquals(expectedRemovedElements, actualRemovedElements);
    }

    @Test
    public void addingAndRemovingLastElement() {
        Integer[] addedElements = {1, 2, 3, 4};
        Integer[] expectedRemovedElements = {3, 2, 4, 1};
        Integer[] actualRemovedElements = new Integer[4];

        list.add(0, addedElements[0]);
        list.add( 1, addedElements[1]);
        list.add( 2, addedElements[2]);
        list.add( 1, addedElements[3]);
        actualRemovedElements[0] = list.remove( 3);
        actualRemovedElements[1] = list.remove( 2);
        actualRemovedElements[2] = list.remove( 1);
        actualRemovedElements[3] = list.remove(0);

        Assert.assertArrayEquals(expectedRemovedElements, actualRemovedElements);
    }

    @Test
    public void addingAndSettingFirstElement() {
        Integer[] addedElements = {1, 2};
        Integer expectedSettedElement = 10;
        Integer expectedRemovedElement = 1;
        Integer actualSettedElement;
        Integer actualRemovedElement;

        list.add(0, addedElements[0]);
        list.add(1, addedElements[1]);
        actualRemovedElement = list.set(0, expectedSettedElement);
        actualSettedElement = list.get(0);

        Assert.assertEquals(expectedRemovedElement, actualRemovedElement);
        Assert.assertEquals(expectedSettedElement, actualSettedElement);
    }

    @Test
    public void addingAndSettingElementsInMiddle() {
        Integer[] addedElements = {1, 2, 3};
        Integer expectedSettedElement = 10;
        Integer expectedRemovedElement = 2;
        Integer actualSettedElement;
        Integer actualRemovedElement;

        list.add(0, addedElements[0]);
        list.add(1, addedElements[1]);
        list.add(2, addedElements[2]);
        actualRemovedElement = list.set( 1, expectedSettedElement);
        actualSettedElement = list.get(1);

        Assert.assertEquals(expectedRemovedElement, actualRemovedElement);
        Assert.assertEquals(expectedSettedElement, actualSettedElement);
    }


    @Test
    public void addingAndSettingLastElement() {
        Integer[] addedElements = {1, 2};
        Integer expectedSettedElement = 10;
        Integer expectedRemovedElement = 2;
        Integer actualSettedElement;
        Integer actualRemovedElement;

        list.add(0, addedElements[0]);
        list.add(1, addedElements[1]);
        actualRemovedElement = list.set(1, expectedSettedElement);
        actualSettedElement = list.get(1);

        Assert.assertEquals(expectedRemovedElement, actualRemovedElement);
        Assert.assertEquals(expectedSettedElement, actualSettedElement);
    }

    @Test
    public void checkingToStringIfListIsntEmpty(){
        Integer[] addedElements = {1, 2, 3, 4};
        String expectedString = "[ 1 4 2 3 ]";
        String actualString;

        list.add(0, addedElements[0]);
        list.add( 1, addedElements[1]);
        list.add( 2, addedElements[2]);
        list.add( 1, addedElements[3]);
        actualString = list.toString();

        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void checkingToStringIfListIsEmpty(){
        String expectedString = "List is empty";
        String actualString;

        actualString = list.toString();

        Assert.assertEquals(expectedString, actualString);
    }

    @Test
    public void checkingResize(){
        Integer[] expectedResizedList = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        Integer[] actualResizedList = new Integer[11];
        int expectedSize = 11;
        int actualSize;

        for (int i = 0; i < expectedSize; i++) {
            list.add(i, expectedResizedList[i]);
        }
        for (int i = 0; i < expectedSize; i++) {
            actualResizedList[i] = list.get(i);
        }
        actualSize = list.size();

        Assert.assertArrayEquals(expectedResizedList, actualResizedList);
        Assert.assertEquals(expectedSize, actualSize);
    }
}
