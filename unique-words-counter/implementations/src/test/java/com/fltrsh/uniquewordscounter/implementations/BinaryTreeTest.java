package com.fltrsh.uniquewordscounter.implementations;

import com.epm.lab.collections.Map;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Iterator;

import static org.junit.Assert.*;

public class BinaryTreeTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void puttingAndGettingSingleElement() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertEquals(1, tree.size());
        assertEquals(1, (int) tree.get(1));
    }

    @Test
    public void puttingAndGettingTwoElements() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertEquals(1, tree.size());
        //WHEN
        tree.put(3, 4);
        //THEN
        assertEquals(2, tree.size());
        assertEquals(1, (int) tree.get(1));
        assertEquals(4, (int) tree.get(3));
    }

    @Test
    public void puttingAndGettingFourElementsToRight() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertEquals(1, tree.size());
        //WHEN
        tree.put(3, 4);
        assertEquals(2, tree.size());
        //WHEN
        tree.put(2, 5);
        //THEN
        assertEquals(3, tree.size());
        //WHEN
        tree.put(4, 10);
        //THEN
        assertEquals(4, tree.size());
        assertEquals(1, (int) tree.get(1));
        assertEquals(5, (int) tree.get(2));
        assertEquals(4, (int) tree.get(3));
        assertEquals(10, (int) tree.get(4));
    }

    @Test
    public void puttingAndGettingFourElementsToLeft() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(5, 1);
        //THEN
        assertEquals(1, tree.size());
        //WHEN
        tree.put(1, 4);
        assertEquals(2, tree.size());
        //WHEN
        tree.put(3, 5);
        //THEN
        assertEquals(3, tree.size());
        //WHEN
        tree.put(2, 10);
        //THEN
        assertEquals(4, tree.size());
        assertEquals(1, (int) tree.get(5));
        assertEquals(4, (int) tree.get(1));
        assertEquals(5, (int) tree.get(3));
        assertEquals(10, (int) tree.get(2));
    }

    @Test
    public void overrideValue() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertEquals(1, tree.size());
        assertEquals(1, (int) tree.get(1));
        //WHEN
        tree.put(4, 2);
        //THEN
        assertEquals(2, tree.size());
        assertEquals(2, (int) tree.get(4));
        //WHEN
        tree.put(4, 123);
        //THEN
        assertEquals(2, tree.size());
        assertEquals(123, (int) tree.get(4));
    }

    @Test
    public void gettingUnexistingElementThatHasBiggerKeyThanInRoot() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertNull(tree.get(2));
    }

    @Test
    public void gettingUnexistingElementThatHasLesserKeyThanInRoot() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN
        tree.put(1, 1);
        //THEN
        assertNull(tree.get(-1));
    }

    @Test
    public void puttingElementWithNullValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = 1 value = null");
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN THEN
        tree.put(1, null);
    }

    @Test
    public void puttingElementWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = null value = 1");
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN THEN
        tree.put(null, 1);
    }

    @Test
    public void puttingElementWithNullKeyAndValue() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. One of the arguments is null: key = null value = null");
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN THEN
        tree.put(null, null);
    }

    @Test
    public void gettingElementWithNullKey() {
        //GIVEN
        expectedException.expect(NullPointerException.class);
        expectedException.expectMessage("Invalid input. The key is null.");
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        //WHEN THEN
        tree.get(null);
    }

    @Test
    public void testingIterator() {
        //GIVEN
        BinaryTree<Integer, Integer> tree = new BinaryTree<>();
        int key = 1;
        int value = 1;
        //WHEN
        tree.put(1, 1);
        tree.put(2, 2);
        tree.put(3, 3);
        tree.put(4, 4);
        tree.put(5, 5);
        Iterator<Map.Entry<Integer, Integer>> iterator = tree.iterator();
        Map.Entry<Integer, Integer> entry;

        //THEN
        while (iterator.hasNext()) {
            entry = iterator.next();
            assertEquals(key++, (int) entry.key);
            assertEquals(value++, (int) entry.value);
        }
    }
}
