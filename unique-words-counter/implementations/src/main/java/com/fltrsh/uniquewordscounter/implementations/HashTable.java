package com.fltrsh.uniquewordscounter.implementations;

import com.epm.lab.collections.Map;

import java.util.Iterator;

import com.fltrsh.uniquewordscounter.interfaces.List;

public class HashTable<K, V> implements Map<K, V> {

    private Node<K, V>[] table;
    private int size;
    private int capacity = 16;
    private final double LOAD_FACTOR = 0.75;

    public HashTable() {
        table = (Node<K, V>[]) new Node[capacity];
        size = 0;
    }
    public HashTable(Map<K,V> map) {
        table = (Node<K, V>[]) new Node[capacity];
        size = 0;
        for(Entry<K,V> entry : map){
            this.put(entry.key, entry.value);
        }
    }


    private static class Node<K, V> {
        Entry<K, V> entry;
        Node<K, V> next;

        Node(K key, V value) {
            entry = new Entry<>(key, value);
        }

    }

    @Override
    public V get(K key) {
        checkArgumentForGet(key);
        int hash = getHash(key);
        Node<K, V> bucket = table[hash];
        if (bucket == null) {
            return null;
        }

        do {
            if (bucket.entry.key.equals(key)) {
                return bucket.entry.value;
            }
            bucket = bucket.next;
        } while (bucket != null);

        return null;
    }

    private void checkArgumentForGet(K key) {
        if (key == null) {
            throw new NullPointerException("Invalid input. The key is null.");
        }
    }

    @Override
    public void put(K key, V value) {
        checkArgumentsForPut(key, value);
        if (size > (int) (capacity / LOAD_FACTOR)) {
            resize();
        }
        putInTable(key, value);
    }

    private void putInTable(K key, V value) {
        int hash = getHash(key);
        Node<K, V> bucket = table[hash];
        if (bucket == null) {
            table[hash] = new Node<>(key, value);
            size++;
        } else {
            Node<K, V> previous;
            do {
                previous = bucket;
                if (bucket.entry.key.equals(key)) {
                    bucket.entry.value = value;
                    return;
                }
                bucket = bucket.next;
            } while (bucket != null);
            bucket = previous;
            bucket.next = new Node<>(key, value);
            size++;
        }
    }

    private int getHash(K key) {
        int hash = key.hashCode() % capacity;
        return hash >= 0 ? hash : -hash;
    }

    private void resize() {
        capacity = (int) (capacity * 2);
        Node<K, V>[] oldTable = table;
        table = (Node<K, V>[]) new Node[capacity];
        recreateTable(table, oldTable);
    }

    private void recreateTable(Node<K, V>[] table, Node<K, V>[] oldTable) {
        size = 0;
        for (int i = 0; i < (capacity / 2); i++) {
            Node<K, V> node = oldTable[i];
            if (node == null) {
                continue;
            }
            while (node != null) {
                this.put(node.entry.key, node.entry.value);
                node = node.next;
            }
        }
    }

    private void checkArgumentsForPut(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Invalid input. One of the arguments is null: key = " + key + " value = " + value);
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        Iterator<Entry<K, V>> iterator = new Iterator<Entry<K, V>>() {
            private int count = 0;
            List<Entry<K, V>> entries;

            @Override
            public boolean hasNext() {
                if (entries == null) {
                    findAllEntries();
                }
                return count != 0;
            }

            @Override
            public Entry<K, V> next() {
                if (entries == null) {
                    findAllEntries();
                }
                count--;
                return entries.remove(0);
            }

            private void findAllEntries() {
                entries = new IndexedList<>();
                for (int i = 0; i < capacity; i++) {
                    Node<K, V> node = table[i];
                    if (node == null) {
                        continue;
                    }
                    while (node != null) {
                        entries.add(entries.size(), node.entry);
                        node = node.next;
                        count++;
                    }
                }
            }
        };
        return iterator;
    }
}
