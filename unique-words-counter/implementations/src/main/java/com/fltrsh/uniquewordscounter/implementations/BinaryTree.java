package com.fltrsh.uniquewordscounter.implementations;

import com.epm.lab.collections.Map;
import com.epm.lab.collections.OrderedMap;

import java.util.Iterator;

import com.fltrsh.uniquewordscounter.interfaces.List;


public class BinaryTree<K extends Comparable<K>, V> implements OrderedMap<K, V> {
    private Node<K, V> root;
    private int size;

    private static class Node<K, V> {

        K key;
        V value;
        Node<K, V> leftChild;
        Node<K, V> rightChild;

        Node(K key, V value) {
            this.key = key;
            this.value = value;
        }


    }

    public BinaryTree() {
        root = null;
        size = 0;
    }

    public BinaryTree(Map<K,V> map) {
        for(Entry<K,V> entry : map){
            this.put(entry.key, entry.value);
        }
    }

    @Override
    public V get(K key) {
        checkArgumentForGet(key);
        if(root == null){
            return null;
        }
        return getFromTree(root, key);
    }

    private void checkArgumentForGet(K key) {
        if (key == null) {
            throw new NullPointerException("Invalid input. The key is null.");
        }
    }

    private V getFromTree(Node<K, V> currentNode, K key) {
        int comparisonResult = key.compareTo(currentNode.key);
        if (comparisonResult == 0) {
            return currentNode.value;
        }
        if (comparisonResult > 0) {
            if (currentNode.rightChild != null) {
                return getFromTree(currentNode.rightChild, key);
            } else {
                return null;
            }
        }
        if (comparisonResult < 0) {
            if (currentNode.leftChild != null) {
                return getFromTree(currentNode.leftChild, key);
            } else {
                return null;
            }
        }
        return null;
    }

    @Override
    public void put(K key, V value) {
        checkArgumentsForPut(key, value);
        if (root == null) {
            root = new Node<>(key, value);
            size++;
        } else {
            putInTree(root, key, value);
        }
    }

    private void checkArgumentsForPut(K key, V value) {
        if (key == null || value == null) {
            throw new NullPointerException("Invalid input. One of the arguments is null: key = " + key + " value = " + value);
        }
    }

    private void putInTree(Node<K, V> currentNode, K key, V value) {
        int comparisonResult = key.compareTo(currentNode.key);
        if (comparisonResult == 0) {
            currentNode.value = value;
            return;
        }

        if (comparisonResult > 0) {
            if (currentNode.rightChild != null) {
                putInTree(currentNode.rightChild, key, value);
            } else {
                currentNode.rightChild = new Node<>(key, value);
                size++;
                return;
            }
        }

        if (comparisonResult < 0) {
            if (currentNode.leftChild != null) {
                putInTree(currentNode.leftChild, key, value);
            } else {
                currentNode.leftChild = new Node<>(key, value);
                size++;
            }
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<Map.Entry<K, V>> iterator() {
        class TreeIterator implements Iterator<Map.Entry<K, V>> {
            private List<Node<K,V>> elements;

            private TreeIterator(Node<K, V> root) {
                elements = new IndexedList<>();
                walkAndAddToList(root);
            }

            private void walkAndAddToList(Node<K, V> currentNode) {
                if (currentNode.leftChild != null) {
                    walkAndAddToList(currentNode.leftChild);
                }
                elements.add(elements.size(),currentNode);
                if (currentNode.rightChild != null) {
                    walkAndAddToList(currentNode.rightChild);
                }
            }

            @Override
            public boolean hasNext() {
                return elements.size() != 0;
            }

            @Override
            public Entry<K, V> next() {
                Node<K,V> node = elements.remove(0);
                Entry<K, V> entry = new Entry<>(node.key, node.value);
                return entry;
            }
        }
        return new TreeIterator(root);
    }
}
