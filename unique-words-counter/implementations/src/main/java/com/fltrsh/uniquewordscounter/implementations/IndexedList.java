package com.fltrsh.uniquewordscounter.implementations;

import com.fltrsh.uniquewordscounter.interfaces.List;

public class IndexedList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private int capacity;
    private int size;
    private Object[] elements;

    public IndexedList() {
        capacity = DEFAULT_CAPACITY;
        size = 0;
        elements = new Object[capacity];
    }

    public IndexedList(int initialCapacity) {
        capacity = initialCapacity;
        size = 0;
        elements = new Object[capacity];
    }

    @Override
    public void add(int index, T element) throws IllegalArgumentException {
        checkElementForNull(element);
        checkIndexForAdd(index);

        if (index == size) {
            addLast(element);
            return;
        }
        addInMiddle(index, element);
    }

    private void checkElementForNull(T element) {
        if(element == null){
            throw new IllegalArgumentException("You cannot insert null element.");
        }
    }

    private void checkIndexForAdd(int index) throws IllegalArgumentException {
        if (index < 0) {
            throw new IllegalArgumentException("Incorrect index = " + index + ", because index < 0 ");
        }
        if (index > size) {
            throw new IllegalArgumentException("Incorrect index = " + index + ", because size = " + size);
        }
    }

    private void checkIndex(int index) throws IllegalArgumentException {
        if (index < 0) {
            throw new IllegalArgumentException("Incorrect index = " + index + ", because index < 0 ");
        }
        if (index >= size) {
            throw new IllegalArgumentException("Incorrect index = " + index + ", because size = " + size);
        }
    }

    private void addInMiddle(int index, T element) {
        checkCapacity();
        makeHoleAt(index);//making hole in array
        elements[index] = element;//and inserting element into that hole
        size++;
    }

    private void makeHoleAt(int index) {
        for (int i = size; i > index; i--) {
            elements[i] = elements[i - 1];
        }
    }

    public void addLast(T element) {
        checkCapacity();
        elements[size++] = element;
    }

    private void checkCapacity() {
        if (size + 1 == capacity) {
            resize();
        }
    }

    private void resize() {
        capacity = (capacity * 3) / 2 + 1;
        Object[] temp = elements;
        elements = new Object[capacity];

        for (int i = 0; i < size; i++) {
            elements[i] = temp[i];
        }
    }



    @Override
    public int size() {
        return size;
    }

    @Override
    public T get(int index) throws IllegalArgumentException {
        checkIndex(index);
        return (T) elements[index];
    }

    @Override
    public T remove(int index) throws IllegalArgumentException {
        checkIndex(index);
        T removedElement = (T) elements[index];
        removeHoleAt(index);
        size--;
        return removedElement;
    }

    private void removeHoleAt(int index) {
        for (int i = index; i < size - 1; i++) {
            elements[i] = elements[i + 1];
        }
    }

    @Override
    public T set(int index, T element) {
        checkIndex(index);
        T removedElement = (T) elements[index];
        elements[index] = element;

        return removedElement;
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "List is empty";
        }
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("[ ");

        for (int i = 0; i < size; i++) {
            stringBuilder.append(elements[i].toString() + " ");
        }

        stringBuilder.append("]");
        return stringBuilder.toString();
    }
}
