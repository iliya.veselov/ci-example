package com.fltrsh.uniquewordscounter.interfaces;


public interface List <T> {
    void add(int index, T element) throws IllegalArgumentException;

    T get(int index) throws IllegalArgumentException;

    T remove(int index) throws IllegalArgumentException;

    T set(int index, T element) throws IllegalArgumentException;

    int size();
}