package com.fltrsh.uniquewordscounter.core;

import com.epm.lab.collections.Map;
import com.fltrsh.uniquewordscounter.implementations.HashTable;
import com.fltrsh.uniquewordscounter.implementations.BinaryTree;

public class UniqueWordsCounter {
    public enum MapType {
        ORDERED,
        UNORDERED;
    }

    int count = 0;
    private Map<String, Integer> uniqueWordsCount;


    public UniqueWordsCounter(MapType type) {
        switch (type) {
            case ORDERED:
                uniqueWordsCount = new BinaryTree<>();
                break;
            case UNORDERED:
                uniqueWordsCount = new HashTable<>();
                break;
        }
    }

    public void addLineOfWords(String line) {
        String[] words = line.replaceAll("[\\]\\[\\?!/:()\\.,«`»\\d–-]+", "").split("[.,\\s]+");
        for (String word : words) {
            addWord(word.toLowerCase());
        }
    }

    private void addWord(String word) {
        Integer wordsCount = uniqueWordsCount.get(word);
        if (wordsCount == null) {
            uniqueWordsCount.put(word, 1);
            count++;
        } else {
            uniqueWordsCount.put(word, wordsCount + 1);
            count++;
        }
    }

    public void outputUniqueWordsCount() {
        System.out.println("Unique words count = " + uniqueWordsCount.size());
        for (Map.Entry<String, Integer> entry : uniqueWordsCount) {
            System.out.println(entry.key + " -> " + entry.value);

        }
    }
}
