package com.fltrsh.uniquewordscounter.demo;

import com.fltrsh.uniquewordscounter.core.UniqueWordsCounter;

import static com.fltrsh.uniquewordscounter.core.UniqueWordsCounter.MapType;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class Demo {

    @Option(name = "-file", metaVar = "fileToRead", depends = "-order", usage = "File from which words will be taken.", required = true)
    private Path fileToRead;
    @Option(name = "-order", metaVar = "mapType", depends = "-file", usage = "Type of output.")
    private MapType mapType;


    public static void main(String[] args) {
        try{
            Demo demo = new Demo();
            demo.doMain(args);
        } catch (IOException e) {
            System.out.println("Error occurred: " + e.getMessage());
        }
    }

    private void doMain(String[] args) throws IOException {
        if (setUpArgs(args)) {
            return;
        }
        UniqueWordsCounter counter = new UniqueWordsCounter(mapType);
        Stream<String> lines = Files.lines(fileToRead);
        lines.forEach(counter::addLineOfWords);
        counter.outputUniqueWordsCount();
    }

    private boolean setUpArgs(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        if (args.length == 0) {
            System.out.println("Unique words counter. Usage:");
            parser.printUsage(System.out);
            return true;
        } else {
            try {
                parser.parseArgument(args);
            } catch (CmdLineException e) {
                System.err.println(e.getLocalizedMessage());
                return true;
            }
        }
        return false;
    }
}
