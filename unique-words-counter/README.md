# homework_11
Contains unique-words-counter app, that can find unique words and show their count.

### Installing
To build project execute in cmd: 
> mvn clean install

### Running
Application runs through command line with command line args:
> -file <path_to_file>
> -order ordered/unordered
To see usage execute application without args.

#### To parse logs (and save result if you want) execute in cmd:

> java -jar .\demo\target\unique-words-counter-demo-jar-with-dependencies.jar -file *path_to__file* -order *ordered/unordered*

##### Example:
> java -jar .\demo\target\unique-words-counter-demo-jar-with-dependencies.jar -file capital_karl_marx.txt -order ordered

### To use binary artifacts:
<?xml version="1.0" encoding="UTF-8" ?>
<settings xsi:schemaLocation='http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd'
          xmlns='http://maven.apache.org/SETTINGS/1.0.0' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>
    
    <profiles>
        <profile>
            <repositories>
                <repository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-fltrsh-homework</id>
                    <name>bintray</name>
                    <url>https://dl.bintray.com/fltrsh/homework</url>
                </repository>
            </repositories>
            <pluginRepositories>
                <pluginRepository>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>bintray-fltrsh-homework</id>
                    <name>bintray-plugins</name>
                    <url>https://dl.bintray.com/fltrsh/homework</url>
                </pluginRepository>
            </pluginRepositories>
            <id>bintray</id>
        </profile>
    </profiles>
    <activeProfiles>
        <activeProfile>bintray</activeProfile>
    </activeProfiles>
</settings>